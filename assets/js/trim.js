(function($) {
    $(document).ready(function() {
        // Get the text field element by its ID
        var textField = document.getElementById('edit-mobile--2');

        // Add an event listener to the text field
        textField.addEventListener('paste', function() {
            // Get the pasted text from the clipboard
            var pastedText = event.clipboardData.getData('text');

            // Remove all whitespace characters from the pasted text
            var trimmedText = pastedText.replace(/\s+/g, '').replace(/\+91/g, '');

            // Set the trimmed text back to the text field
            textField.value = trimmedText;
        });
    });
})(jQuery);